# encoding: utf-8


def has_route(context):
    return context.route in [
        "/hello",
        "/hello.txt",
    ]


def request(context):
    if context.route == "/hello":
        answer = f"0hello\t/hello.txt\t{context.hostname}\t{context.port}\r\n.\r\n"
        return True, answer.encode("latin-1"), "1"
    else:
        return True, "hello world!.\r\n".encode("latin-1"), "0"
